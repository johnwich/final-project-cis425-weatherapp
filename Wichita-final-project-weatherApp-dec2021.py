#John Wichita
#12-5-2021
#Bellevue University
#Python programming with Prof. Mack
#final class project weather app

from bs4 import BeautifulSoup, FeatureNotFound
import requests
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

def main(city):
    #if index error return "that is not a valid input, please run the program again.""
    city = city.replace(" ", "+")
    res = requests.get(
        f'https://www.google.com/search?q={city}&sourceid=chrome&ie=UTF-8', headers=headers)
        
    print("Searching...\n")
    soup = BeautifulSoup(res.text, 'html.parser')
    # print(soup)
    location = soup.select('#wob_loc')[0].getText().strip()
    time = soup.select('#wob_dts')[0].getText().strip()
    info = soup.select('#wob_dc')[0].getText().strip()
    weather = soup.select('#wob_tm')[0].getText().strip()
    print(location)
    print(time)
    print(info)
    print(weather+"°F")
    

 
if __name__ == "__main__":
    flag = False 
    while flag == False:
        city = input("Enter the Name of City or a zip code:  ")
        city = city+" weather"
        try:
            main(city)
        except IndexError:
            print("sorry, there must be an error, please check your spelling and try the program again.")
        choice = input("\nWould you like to get another weather forcast? y/n: ")

        if choice == "y":
            flag = False
        else:
            flag = True
            print("\nThank you for using this program. Have a nice day! \n")
